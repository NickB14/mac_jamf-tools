
#!/bin/bash
#Edited by Nick Baird for Lubbock Cooper ISD
#April 2020
#
JAMF_BINARY="/usr/local/bin/jamf"

ComputerName=`/usr/bin/osascript <<EOT
tell application "System Events"
activate
set ComputerName to text returned of (display dialog "Please Input New Computer Name" default answer "" with icon 2)
end tell
EOT`

AssetTag=`/usr/bin/osascript <<EOT
tell application "System Events"
activate
set AssetTag to text returned of (display dialog "What is the computer asset tag #" default answer "" with icon 2)
end tell
EOT`


Campus=`/usr/bin/osascript <<EOT
tell application "System Events"
activate
set CampusList to {"Administration", "Central Elementary", "DEAP", "East Elementary", "L Bush Middle School", "LC High School", "LC Middle School", "LC New Hope Academy", "North Elementary", "South Elementary", "Special ED", "Transportation", "West Elementary"}
set Campus to choose from list CampusList with prompt "Select your location"
end tell
EOT`

Role=`/usr/bin/osascript <<EOT
tell application "System Events"
activate
set RoleList to {"Staff", "Student"}
set Role to choose from list CampusList with prompt "Who is this computer for?"
end tell
EOT`

# Logic to set new computer name
scutil --set HostName $ComputerName
scutil --set LocalHostName $ComputerName
scutil --set ComputerName $ComputerName
$JAMF_BINARY" setComputerName -name "$ComputerName"

#logic for location
"$JAMF_BINARY" recon -building "$Campus"

#Logic for Department
"$JAMF_BINARY" recon -department "$Role"

#Logic for asset tag
$JAMF_BINARY" recon -assetTag "$AssetTag"

