#!/bin/bash

#Downloads the full installer of the most recent MacOS to the Applications folder  
/usr/sbin/softwareupdate --fetch-full-installer

#Sends command to jamf to clear the policy logs for the computer this is run on so that when the OS is reinstalled it will install all the scoped policies and configurations
jamf flushPolicyHistory

#pauses the script for 5 seconds
sleep 10s

#Runs the installer application, agrees to everything, wipes the HD and reinistalled the OS
 '/Applications/Install macOS Catalina.app/Contents/Resources/startosinstall' --eraseinstall --agreetolicense --forcequitapps --newvolumename 'Macintosh HD'  

exit 0